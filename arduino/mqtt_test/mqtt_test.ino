#include<WiFiClient.h>
#include<ESP8266WiFi.h>
#include<ESP8266HTTPClient.h>
#include<PubSubClient.h>
#include"Adafruit_BMP085.h"
#include "DHT.h"

#define MQ2 A0
#define MOTOR D4
#define BULB  D5
#define dht_dpin D3 // 0
#define LED D6
#define BUZZER D7
#define DHTTYPE DHT11
DHT dht(dht_dpin, DHTTYPE);

Adafruit_BMP085 bmp;

WiFiClient espClient;
PubSubClient client(espClient); 

const char *ssid = "Vru";
char const *password = "vrush1397";


void on_message(char*topic,byte*payload,unsigned int length)
{
  String buff;
  for(int i=0;i<2;i++)
    buff += (char)payload[i];
  buff+='\0';
  
  Serial.println("message received !");
  if(!strcmp(topic,"motor"))
  {
    if(!buff.compareTo("ON"))
    {
      digitalWrite(MOTOR,LOW); 
    }
    else if(!buff.compareTo("OF"))
    {
      digitalWrite(MOTOR,HIGH);
    }   
  }
  else if(!strcmp(topic,"bulb"))
  {
    if(!buff.compareTo("ON"))
    {
      digitalWrite(BULB,LOW); 
    }
    else if(!buff.compareTo("OF"))
    {
      digitalWrite(BULB,HIGH);
    }
  }
}


void setup() {

  Serial.flush();
  Serial.begin(115200);
  dht.begin();
  pinMode(MOTOR,OUTPUT);
  pinMode(BULB,OUTPUT);
  pinMode(LED,OUTPUT);
  pinMode(MQ2,INPUT);
  pinMode(D0,INPUT);
  pinMode(BUZZER,OUTPUT);

  digitalWrite(MOTOR,HIGH);
  digitalWrite(BULB,HIGH);

  WiFi.mode(WIFI_STA);
  WiFi.begin(ssid, password);

  if (!bmp.begin()) {
    Serial.println("Could not find a valid BMP085 sensor, check wiring!");
    while (1) {}
  }

  Serial.print("Connecting.");
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(700);
    Serial.print(".");
  }
  client.setServer("192.168.43.146",1883);
  client.setCallback(on_message);
  Serial.println("Connected...");
  Serial.print("IP Address:");
  Serial.println(WiFi.localIP());
  delay(2000);
  
}

void loop() {
  while(!client.connected())
  {
    if (client.connect("NodeMCU_base"))
    {  
      while(!(client.subscribe("bulb") && client.subscribe("motor"))); 
      Serial.println("subscribed!");
    }
    else
    {
      Serial.print(".");
      delay(500);
    } 
  }
 
  // Wait a few seconds between measurements.
  client.loop();
  
  //set threshold for mq2
  int th = 200;
  //read MQ2 sensor output
  int mq2 = analogRead(MQ2);
  Serial.print("MQ2_A: ");
  Serial.println(mq2);
  
  //sensor sense the smoke and glow the led
  if(mq2 > th)
  digitalWrite(BUZZER,HIGH);
  else
  digitalWrite(BUZZER,LOW);

  //Read pressure
  float p = bmp.readPressure();
  // Read humidity
  float h = dht.readHumidity();
  // Read temperature as Celsius (the default)
  float t = dht.readTemperature();

  // Check if any reads failed and exit early (to try again).
  if (isnan(h) || isnan(t) ) {
    Serial.println("Failed to read from DHT sensor!");;
    return;
  }

  // set the threshold temperature to beep the buzzer
  if(t >= 34)
  {
    digitalWrite(LED,HIGH);
  }else{
      digitalWrite(LED,LOW);
  }

  String body = "{ \"tempC\":" + String(t) + "," + "\"pressure\":" + String(p) + "," + "\"humidity\":" + String(h) + "}";
  Serial.println("sending : " + body);

  HTTPClient httpClient;

  httpClient.begin("http://192.168.43.146:4500/dht11_data");

  httpClient.addHeader("Content-Type", "application/json");

  int statusCode = httpClient.POST(body);

  Serial.println("status code: " + String(statusCode));
  httpClient.end();

  delay(1000);

}
