from flask import Flask, jsonify, request
import mysql.connector
import paho.mqtt.client as mqtt

app = Flask(__name__)
client = mqtt.Client()

def on_connect():
    print("connected")

client.on_connect = on_connect
client.connect("192.168.43.146",1883,60)

@app.route("/", methods=["GET"])
def root():
    return "welcome to the application"

@app.route("/dht11_data", methods=["POST"])
def post_dht11_data():
    temperature = request.json.get("tempC")
    humidity = request.json.get("humidity")
    pressure = request.json.get("pressure")
    connection = mysql.connector.connect(host="localhost", user="root", password="Sunbeam@123", database="project")
    cursor = connection.cursor()
    statement = f"insert into sensor_data (temperature_value,pressure,humidity_value) values ({temperature},{pressure},{humidity})"
    cursor.execute(statement)
    connection.commit()
    cursor.close()
    connection.close()
    return "inserted new temperature"


@app.route("/dht11_data", methods=["GET"])
def get_dht11_data():
    connection = mysql.connector.connect(host="localhost", user="root", password="Sunbeam@123", database="project")
    cursor = connection.cursor()
    statement = "select id,temperature_value,pressure,humidity_value,timestamp from sensor_data"
    cursor.execute(statement)
    data = cursor.fetchall()
    sensor_data = []
    for (id,temperature_value,pressure,humidity_value,timestamp) in data:
        values={
            "id": id,
            "temperature_value": temperature_value,
            "pressure": pressure,
            "humidity_value": humidity_value,
            "timestamp": timestamp
        }
        sensor_data.append(values)
    cursor.close()
    connection.close()
    return jsonify(sensor_data)

@app.route("/motor_on")
def motor_on():
    client.publish("motor",payload="ON")
    return "motor_on"

@app.route("/motor_off")
def motor_off():
    client.publish("motor",payload="OFF")
    return "motor_off"

@app.route("/bulb_on")
def bulb_on():
    client.publish("bulb",payload="ON")
    return "bulb ON"

@app.route("/bulb_off")
def bulb_off():
    client.publish("bulb",payload="OFF")
    return "bulb_off"

if __name__ == '__main__':
    app.run(port=4500,host="192.168.43.146",debug="true")