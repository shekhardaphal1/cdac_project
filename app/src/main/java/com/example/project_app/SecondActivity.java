package com.example.project_app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.speech.RecognizerIntent;
import android.speech.RecognizerResultsIntent;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.Switch;
import android.widget.Toast;

import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;

public class SecondActivity extends Activity implements CompoundButton.OnCheckedChangeListener {

    Switch motor,bulb;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_second);

       motor = findViewById(R.id.motor);
       motor.setOnCheckedChangeListener(this);
       bulb = findViewById(R.id.bulb);
       bulb.setOnCheckedChangeListener(this);
    }

    private void callAPI(String path){
        //Server url
        String url = "http://192.168.43.146:4500/"+path;
        Log.e("SecondActivity","Calling Rest API: "+url);

        //make the call
        Ion.with(this)
                .load(url)
                .asString()
                .setCallback(new FutureCallback<String>() {
                    @Override
                    public void onCompleted(Exception e, String result) {
                        Log.e("SecondActivity",result);
                        Toast.makeText(SecondActivity.this, "result: "+result, Toast.LENGTH_SHORT).show();
                    }
                });
    }

    @Override
    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
        String path = "";
        if (buttonView==bulb){
            // check if bulb is on or off
            if (bulb.isChecked()) {
                path="bulb_on";
            } else {
                path="bulb_off";
            }
        }else if(buttonView==motor){
            // check if motor is on or off
            if(motor.isChecked()){
                path ="motor_on";
            }else {
                path="motor_off";
            }
        }
        //call API
        callAPI(path);
    }

    public void onVoiceCommand(View view) {
        Intent intent = new Intent();
        intent.setAction(RecognizerIntent.ACTION_RECOGNIZE_SPEECH);
        intent.putExtra(RecognizerIntent.EXTRA_LANGUAGE_MODEL,RecognizerIntent.LANGUAGE_MODEL_FREE_FORM);
        intent.putExtra(RecognizerIntent.EXTRA_PROMPT,"Your Command");
        startActivityForResult(intent,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data)
    {
        super.onActivityResult(requestCode, resultCode, data);
        if(data != null)
        {
            ArrayList<String> results = data.getStringArrayListExtra(RecognizerIntent.EXTRA_RESULTS);
            if(results.size() > 0)
            {
                String result = results.get(0);
                Log.e("SecondActivity","Command: "+result);
                if (result.contains("on"))
                {
                    if (result.contains("bulb"))
                    {
                        bulb.setChecked(true);
                        callAPI("bulb_on");
                    }
                    else if (result.contains("motor"))
                    {
                        motor.setChecked(true);
                        callAPI("motor_on");
                    }
                }
                else if (result.contains("off") || result.contains("of"))
                {
                    if (result.contains("bulb"))
                    {
                        bulb.setChecked(false);
                        callAPI("bulb_off");
                    }
                    else if (result.contains("motor"))
                    {
                        motor.setChecked(false);
                        callAPI("motor_off");
                    }
                }
            }
        }
    }
}

