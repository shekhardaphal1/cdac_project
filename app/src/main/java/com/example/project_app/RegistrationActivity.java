package com.example.project_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

public class RegistrationActivity extends AppCompatActivity {

    EditText editName, editEmail, editPassword, editConfirmPassword, editPhone;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_registration);

        editName = findViewById(R.id.editName);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
        editConfirmPassword = findViewById(R.id.editConfirmPassword);
        editPhone = findViewById(R.id.editPhone);
    }

    public void onRegister(View v){
        String name = editName.getText().toString();
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();
        String confirmPassword = editConfirmPassword.getText().toString();
        String phone = editPhone.getText().toString();

        if(name.length()==0){
            editName.setError("Name is mandatory");
        }else if(email.length()==0){
            editEmail.setError("Emai. is mandatory");
        }else if(password.length()==0){
            editPassword.setError("Paassword is mandatory");
        }else if(confirmPassword.length()==0){
            editConfirmPassword.setError("Confirm the password");
        }else if(phone.length()==0){
            editPhone.setError("Phone number is mandatory");
        }else if(!password.equals(confirmPassword)){
            Toast.makeText(this, "Password does not match", Toast.LENGTH_SHORT).show();
        }else {
            DBHelper helper = new DBHelper(this);
            SQLiteDatabase db = helper.getWritableDatabase();
            ContentValues values = new ContentValues();
            values.put("userName", name);
            values.put("email",email);
            values.put("password",password);
            values.put("phone",phone);
            db.insert("user",null,values);
            db.close();
            Toast.makeText(this, "You are successfully registered", Toast.LENGTH_SHORT).show();
            finish();
        }
    }

    public void onCancel(View v){
        finish();
    }
}
