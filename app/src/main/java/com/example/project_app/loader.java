package com.example.project_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;

public class loader extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_loader);

        new Thread(new Runnable() {
            @Override
            public void run() {
                try {
                    Thread.sleep(3000);

                    SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(loader.this);

                    boolean isLoggedIn = preferences.getBoolean("isLoggedIn",false);

                    if(isLoggedIn){
                        Intent intent = new Intent(loader.this,FirstActivity.class);
                        startActivity(intent);
                    } else{
                        Intent intent = new Intent(loader.this, LoginActivity.class);
                        startActivity(intent);
                    }
                    finish();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }).start();
    }
}
