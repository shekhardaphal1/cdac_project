package com.example.project_app;

import androidx.annotation.NonNull;

public class Sensor_data {
    int id;
    float temperature_value;
    float pressure;
    float humidity_value;
    String timestamp;

    @NonNull
    @Override
    public String toString() {
        return "id : " + id + ",\n temperature_value : " + temperature_value + "(in C)" + ",\n pressure : " + pressure + "(in P)" + ",\n humidity_value : " + humidity_value + "(in %)" +",\n timestamp : " + timestamp;
    }

    public Sensor_data(){

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public float getTemperature_value() {
        return temperature_value;
    }

    public void setTemperature_value(float temperature_value) {
        this.temperature_value = temperature_value;
    }

    public float getPressure() {
        return pressure;
    }

    public void setPressure(float pressure) {
        this.pressure = pressure;
    }

    public void setHumidity_value(float humidity_value) {
        this.humidity_value = humidity_value;
    }

    public void setTimestamp(String timestamp) {
        this.timestamp = timestamp;
    }

    public String getTimestamp() {
        return timestamp;
    }

    public float getHumidity_value() {
        return humidity_value;
    }
}
