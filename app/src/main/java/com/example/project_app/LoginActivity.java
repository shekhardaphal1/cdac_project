package com.example.project_app;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    EditText editEmail, editPassword;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        editEmail = findViewById(R.id.editEmail);
        editPassword = findViewById(R.id.editPassword);
    }

    public void onLogin(View v){
        String email = editEmail.getText().toString();
        String password = editPassword.getText().toString();

        if (email.length()==0){
            editEmail.setError("Email is mandatory");
        }else if(password.length()==0){
            editPassword.setError("password is mandatory");
        }else{
            DBHelper helper = new DBHelper(this);
            SQLiteDatabase db = helper.getReadableDatabase();

            String columns[] = {"id","userName","email","phone"};
            String selection = "email = '"+email+"' and password = '"+password+"'";
            Cursor cursor = db.query("user",columns,selection,null,null,null,null);

            boolean isSuccessfulLogin = false;
            if(cursor.isAfterLast()){
                Toast.makeText(this, "Invalid user email or password", Toast.LENGTH_SHORT).show();
                isSuccessfulLogin = false;
            }else{
                cursor.moveToFirst();
                int userId = cursor.getInt(0);
                String userName = cursor.getString(1);
                String userEmail = cursor.getString(2);
                String userPhone = cursor.getString(3);

                SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
                preferences
                        .edit()
                        .putString("name",userName)
                        .putBoolean("isLoggedIn",true)
                        .commit();

                Toast.makeText(this, "Successful Login", Toast.LENGTH_SHORT).show();
                isSuccessfulLogin = true;
            }
            cursor.close();
            db.close();

            if(isSuccessfulLogin){
                Intent intent = new Intent(this,FirstActivity.class);
                startActivity(intent);
                finish();
            }
        }
    }
    public void onRegister(View v){
        Intent intent = new Intent(this,RegistrationActivity.class);
        startActivity(intent);
    }

}
