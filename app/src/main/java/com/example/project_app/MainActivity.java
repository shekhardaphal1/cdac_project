package com.example.project_app;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Build;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.JsonArray;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;

import java.util.ArrayList;


public class MainActivity extends AppCompatActivity implements View.OnClickListener {
    ListView listView;
    ArrayList<Sensor_data> sensors_data = new ArrayList<>();
    ArrayAdapter<Sensor_data> adapter;

    Button Refresh,refresh;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Refresh = findViewById(R.id.Refresh);
        refresh = findViewById(R.id.refresh);
        Refresh.setOnClickListener(this);
        refresh.setOnClickListener(this);
        listView = findViewById(R.id.listView);
        adapter = new ArrayAdapter<Sensor_data>(this, android.R.layout.simple_list_item_1, sensors_data){
            @Override
            public View getView(int position, View convertView, ViewGroup parent){
                // Get the Item from ListView
                View view = super.getView(position, convertView, parent);

                // Initialize a TextView for ListView each Item
                TextView tv = view.findViewById(android.R.id.text1);

                // Set the text color of TextView (ListView Item)
                tv.setTextColor(Color.WHITE);

                // Generate ListView Item using TextView
                return view;
            }
        };
        listView.setAdapter(adapter);
    }


    @Override
    public void onClick(View v) {
        if (v == Refresh) {
            load_sensor_data();
        }else if (v == refresh) {
            load_current_sensor_data();
        }
    }

    public void load_sensor_data() {
        sensors_data.clear();
        // create url
        String url = "http://192.168.43.146:4500/dht11_data";

        // call api
        Ion.with(this)
                .load("GET", url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        //  Log.e("MainActivity", result.toString());
                        for (int index = result.size() - 1; index > result.size() - 11 ; index--) {
                            // get json object at every index
                            JsonObject object = result.get(index).getAsJsonObject();
                            // create an object of sensor_data
                            Sensor_data sensor_data = new Sensor_data();
                            // set the properties
                            sensor_data.setId(object.get("id").getAsInt());
                            sensor_data.setTemperature_value(object.get("temperature_value").getAsFloat());
                            sensor_data.setPressure(object.get("pressure").getAsFloat());
                            sensor_data.setHumidity_value(object.get("humidity_value").getAsFloat());
                            sensor_data.setTimestamp(object.get("timestamp").getAsString());

                            // add the sensor_data to the sensors_data list
                            sensors_data.add(sensor_data);
                        }

                        // refresh the list
                        adapter.notifyDataSetChanged();
                    }
                });

    }

    public void load_current_sensor_data(){
        sensors_data.clear();
        // create url
        String url = "http://192.168.43.146:4500/dht11_data";

        // call api
        Ion.with(this)
                .load("GET", url)
                .asJsonArray()
                .setCallback(new FutureCallback<JsonArray>() {
                    @RequiresApi(api = Build.VERSION_CODES.N)
                    @Override
                    public void onCompleted(Exception e, JsonArray result) {
                        //  Log.e("MainActivity", result.toString());
                        for (int index = result.size()-1; index > result.size()-2; index--) {

                            // get json object at every index
                            JsonObject object = result.get(index).getAsJsonObject();
                            // create an object of sensor_data
                            Sensor_data sensor_data = new Sensor_data();
                            // set the properties
                            sensor_data.setId(object.get("id").getAsInt());
                            sensor_data.setTemperature_value(object.get("temperature_value").getAsFloat());
                            sensor_data.setPressure(object.get("pressure").getAsFloat());
                            sensor_data.setHumidity_value(object.get("humidity_value").getAsFloat());
                            sensor_data.setTimestamp(object.get("timestamp").getAsString());


                            // add the sensor_data to the sensors_data list
                            sensors_data.add(sensor_data);
                        }

                        // refresh the list
                        adapter.notifyDataSetChanged();
                    }
                });
    }

    public void onLogout() {
        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(this);
        preferences
                .edit()
                .putBoolean("isLoggedIn", false)
                .commit();
        Intent intent = new Intent(this, LoginActivity.class);
        startActivity(intent);
        finish();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        menu.add("Logout");
        menu.add("Close");
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getTitle().equals("Close")) {
            finish();
        } else if (item.getTitle().equals("Logout")) {
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle("Confirmation");
            builder.setMessage("Are you sure you want to Logout?");
            builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    onLogout();
                }
            });

            builder.setNegativeButton("No", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    dialog.dismiss();
                }
            });
            builder.show();
        }
        return super.onOptionsItemSelected(item);
    }

}
